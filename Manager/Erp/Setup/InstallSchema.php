<?php

namespace Manager\Erp\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Function install
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {
        $installer = $setup;
        $installer->startSetup();
        $table = $installer->getConnection()->newTable(
            $installer->getTable('erp_log')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
            'Entity ID'
        )->addColumn(
            'file_name',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false, 'unique' => true],
            "File Name"
        )->addColumn(
            'file_path',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            "File path"
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            "Created At"
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
            "Updated At"
        )->addColumn(
            'link',
            Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            "Link"
        );
        $installer->getConnection()->createTable($table);
        //START table setup
        //        $table = $installer->getConnection()->newTable(
        //            $installer->getTable('kavinga')
        //        )->addColumn(
        //            'kid',
        //            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
        //            null,
        //            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true,],
        //            'Entity ID'
        //        )->addColumn(
        //            'title',
        //            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        //            255,
        //            ['nullable' => false,],
        //            'Demo Title'
        //        )->addColumn(
        //            'creation_time',
        //            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
        //            null,
        //            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,],
        //            'Creation Time'
        //        )->addColumn(
        //            'update_time',
        //            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
        //            null,
        //            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE,],
        //            'Modification Time'
        //        )->addColumn(
        //            'is_active',
        //            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
        //            null,
        //            ['nullable' => false, 'default' => '1',],
        //            'Is Active'
        //        );
        //        $installer->getConnection()->createTable($table);
        //        //END   table setup
    }
}
