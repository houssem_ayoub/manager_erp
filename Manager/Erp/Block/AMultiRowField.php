<?php

namespace Manager\Erp\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Escaper;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Manager\Erp\Helper\EnvData;

/**
 * @method AbstractElement getElement
 */
abstract class AMultiRowField extends Field
{
    protected array $headers= [];
    protected $_template = 'Manager_Erp::multi_row.phtml';
    protected EnvData $envData;
    protected Escaper $escaper;

    public function __construct(Context $context, Escaper $escaper, EnvData $envData, array $data = [], ?SecureHtmlRenderer $secureRenderer = null)
    {
        parent::__construct($context, $data, $secureRenderer);
        $this->envData = $envData;
        $this->escaper = $escaper;
        $this->initHeaders();
    }
    /**
     * Get html table rows
     * @return string
     */
    abstract public function getHtmlRows():string;

    /**
     * init table header's names
     */
    abstract public function initHeaders():void;

    /**
     * Convert element to html
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        $this->setData("element", $element);
        return $this->_toHtml();
    }
    public function getHtmlId():string
    {
        return $this->escaper->escapeHtmlAttr($this->getElement()->getHtmlId() ?? '_' . uniqid());
    }

    /**
     * Generate Table id
     * @return string
     */
    public function getId():string
    {
        return $this->escaper->escapeHtmlAttr($this->getElement()->getId());
    }

    /**
     * Get input html table row
     * @param $columnName
     * @return string
     */
    protected function getHtmlRow($columnName):string
    {
        $value = $this->headers[$columnName]??$columnName;
        $unique = uniqid();
        return '<tr id="row_' . $unique . '">
                    <td>
                        <label for="' . $this->getInputElementId($unique, $columnName) . '">' . $columnName . '</label>
                    </td>
                    <td>
                        <input type="text" style="min-width: min-content;" id="' . $this->getInputElementId($unique, $columnName) . '" name="' . $this->getInputElementName($columnName) . '" class="required-entry" value="' . $value . '">
                    </td>
                </tr>';
    }

    /**
     * Get name of html input element
     * @param string $columnName
     * @return string
     */
    public function getInputElementName(string $columnName): string
    {
        return $this->escaper->escapeHtmlAttr($this->getElement()->getName() . '[' . $columnName . ']');
    }

    /**
     * Get id of html input element
     * @param string $rowId
     * @param string $columnName
     * @return string
     */
    protected function getInputElementId(string $rowId, string $columnName): string
    {
        return $this->escaper->escapeHtmlAttr($rowId . '_' . $columnName);
    }
}
