<?php

namespace Manager\Erp\Block\Adminhtml;

use Manager\Erp\Block\AMultiRowField;

class InvoiceMultiRow extends AMultiRowField
{

    /**
     * Get html table rows
     * @return string
     */
    public function getHtmlRows():string
    {
        $html = '';
        foreach ($this->envData->getInvoiceExportColumn() as $column) {
            $html .= $this->getHtmlRow($column);
        }
        return $html;
    }

    public function initHeaders(): void
    {
        $this->headers = $this->envData->getInvoiceHeaders();
    }
}
