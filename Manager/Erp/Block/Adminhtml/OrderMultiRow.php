<?php

namespace Manager\Erp\Block\Adminhtml;

use Exception;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Manager\Erp\Block\AMultiRowField;
use Manager\Erp\Helper\EnvData;

class OrderMultiRow extends AMultiRowField
{
    /**
     * Get html table rows
     * @return string
     */
    public function getHtmlRows():string
    {
        $html = '';
        foreach ($this->envData->getOrderExportColumn() as $column) {
            $html .= $this->getHtmlRow($column);
        }
        return $html;
    }
    public function initHeaders(): void
    {
        $this->headers = $this->envData->getOrderHeaders();
    }
}
