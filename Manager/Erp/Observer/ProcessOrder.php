<?php

namespace Manager\Erp\Observer;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Manager\Erp\Helper\EnvData;
use Manager\Erp\Model\Export\CSVSaver;
use Manager\Erp\Model\Export\DataSaver;
use Manager\Erp\Model\Export\JsonSaver;
use Manager\Erp\Model\Export\XMLSaver;

class ProcessOrder implements ObserverInterface
{
    private EnvData $config;
    public function __construct(EnvData $config)
    {
        $this->config = $config;

    }

    protected function getSaver():DataSaver
    {
        $manager = ObjectManager::getInstance();
        return match ($this->config->getExportType()) {
            "CSV" => $manager->get(CSVSaver::class),
            "XML" =>  $manager->get(XMLSaver::class),
            "JSON" => $manager->get(JsonSaver::class)
        };
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        if ($this->config->isEnabled()) {
            if ($this->config->exportOrderWithHeader()) {
                $data['header'] = array_values($this->config->getOrderHeaders());
            }

            $order = $observer->getData("order");
            $data['content'][] = array_map(
                fn ($columnName) => $order->getData($columnName),
                $this->config->getOrderExportColumn()
            );

            $folderName = $this->config->getFolderName();
            $fileName = "order_" . uniqid();
            $this->getSaver()->save([
                "fileName" => $fileName,
                "directory" => $folderName
            ], $data);
        }
    }
}
