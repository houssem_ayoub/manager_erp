<?php

namespace Manager\Erp\Observer;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Manager\Erp\Helper\EnvData;
use Manager\Erp\Model\Export\CSVSaver;
use Manager\Erp\Model\Export\DataSaver;
use Manager\Erp\Model\Export\JsonSaver;
use Manager\Erp\Model\Export\XMLSaver;

class ProcessInvoice implements ObserverInterface
{
    private EnvData $config;

    public function __construct(
        EnvData      $config
    ) {
        $this->config = $config;
    }

    protected function getSaver():DataSaver
    {
        $manager = ObjectManager::getInstance();
        return match ($this->config->getExportType()) {
            "CSV" => $manager->get(CSVSaver::class),
            "XML" =>  $manager->get(XMLSaver::class),
            "JSON" => $manager->get(JsonSaver::class)
        };
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        if ($this->config->isEnabled()) {
            if ($this->config->exportInvoiceWithHeader()) {
                $data['header'] = array_values($this->config->getInvoiceHeaders());
            }

            $invoice = $observer->getData("invoice");
            $data['content'][] = array_map(
                fn ($columnName) => $invoice->getData($columnName),
                $this->config->getInvoiceExportColumn()
            );

            $folderName = $this->config->getFolderName();
            $fileName = "invoice_" . uniqid();
            $this->getSaver()->save([
                "fileName" => $fileName,
                "directory" => $folderName
            ], $data);
        }
    }
}
