<?php

namespace Manager\Erp\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\SerializerInterface;
use Manager\Erp\Model\Config\Source\InvoiceFieldsOption;
use Manager\Erp\Model\Config\Source\InvoiceFieldsOptionFactory;
use Manager\Erp\Model\Config\Source\OrderFieldsOption;
use Manager\Erp\Model\Config\Source\OrderFieldsOptionFactory;

class EnvData extends AbstractHelper
{
    private const XML_ERP_PATH_GENERAL_ENABLED = "erp/general/enabled";
    private const XML_ERP_PATH_GENERAL_FOLDER_NAME = "erp/general/folder_name";
    private const XML_ERP_PATH_GENERAL_EXPORT_EXTENSION = "erp/general/export_extension";
    private const XML_ERP_PATH_ORDER_EXPORT_COLUMNS = "erp/order/export_columns";
    private const XML_ERP_PATH_ORDER_IS_EXPORT_COLUMNS_ALL= "erp/order/is_export_all";
    private const XML_ERP_PATH_ORDER_IS_EXPORT_HEADERS= "erp/order/is_export_headers";
    private const XML_ERP_PATH_INVOICE_EXPORT_COLUMNS = "erp/invoice/export_columns";
    private const XML_ERP_PATH_INVOICE_IS_EXPORT_COLUMNS_ALL= "erp/invoice/is_export_all";
    private const XML_ERP_PATH_INVOICE_IS_EXPORT_HEADERS= "erp/invoice/is_export_headers";
    private const XML_ERP_PATH_ORDER_STYLE_HEADERS = "erp/order_style/column_header";
    private const XML_ERP_PATH_INVOICE_STYLE_HEADERS = "erp/invoice_style/column_header";
    private OrderFieldsOption $orderFieldsOption;
    private InvoiceFieldsOption $invoiceFieldsOption;
    private SerializerInterface $serializer;
    public function __construct(Context $context, OrderFieldsOptionFactory $orderOptionFactory, InvoiceFieldsOptionFactory $invoiceOptionFactory, SerializerInterface $serializer)
    {
        parent::__construct($context);
        $this->orderFieldsOption = $orderOptionFactory->create();
        $this->invoiceFieldsOption = $invoiceOptionFactory->create();
        $this->serializer = $serializer;
    }

    /**
     * Get if Manager_Erp is enabled
     * @return bool
     */
    public function isEnabled():bool
    {
        return $this->scopeConfig->getValue(self::XML_ERP_PATH_GENERAL_ENABLED);
    }

    /**
     * Get export's folder name
     * @return string
     */
    public function getFolderName():string
    {
        return $this->scopeConfig->getValue(self::XML_ERP_PATH_GENERAL_FOLDER_NAME);
    }

    /**
     * Get order column's table to export
     * @return string[]
     */
    public function getOrderExportColumn():array
    {
        if ($this->scopeConfig->getValue(self::XML_ERP_PATH_ORDER_IS_EXPORT_COLUMNS_ALL)) {
            return $this->orderFieldsOption->toArray();
        }
        return explode(",", $this->scopeConfig->getValue(self::XML_ERP_PATH_ORDER_EXPORT_COLUMNS));
    }

    /**
     * Get invoice column's table to export
     * @return string[]
     */
    public function getInvoiceExportColumn():array
    {
        if ($this->scopeConfig->getValue(self::XML_ERP_PATH_INVOICE_IS_EXPORT_COLUMNS_ALL)) {
            return $this->invoiceFieldsOption->toArray();
        }
        return explode(",", $this->scopeConfig->getValue(self::XML_ERP_PATH_INVOICE_EXPORT_COLUMNS));
    }

    /**
     * Get if order header will be exported
     * @return bool
     */
    public function exportOrderWithHeader():bool
    {
        return $this->scopeConfig->getValue(self::XML_ERP_PATH_ORDER_IS_EXPORT_HEADERS);
    }
    /**
     * Get if invoice header will be exported
     * @return bool
     */
    public function exportInvoiceWithHeader():bool
    {
        return $this->scopeConfig->getValue(self::XML_ERP_PATH_INVOICE_IS_EXPORT_HEADERS);
    }

    /**
     * Get Order label's header
     * @return array<string,string>
     */
    public function getOrderHeaders():array
    {
        return $this->serializer->unserialize($this->scopeConfig->getValue(self::XML_ERP_PATH_ORDER_STYLE_HEADERS)??"[]");
    }
    /**
     * Get Invoice label's header
     * @return array<string,string>
     */
    public function getInvoiceHeaders():array
    {
        return $this->serializer->unserialize($this->scopeConfig->getValue(self::XML_ERP_PATH_INVOICE_STYLE_HEADERS)??"[]");
    }

    /**
     * Get exported file type
     * @return string (CSV | XML| JSON)
     */
    public function getExportType():string
    {
        return match ($this->scopeConfig->getValue(self::XML_ERP_PATH_GENERAL_EXPORT_EXTENSION)) {
            1 => "XML",
            2 => "JSON",
            default => "CSV",
        };
    }
}
