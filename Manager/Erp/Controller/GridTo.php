<?php

namespace Manager\Erp\Controller;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory as InvoiceCollectionFactory;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Ui\Component\MassAction\Filter;
use Manager\Erp\Helper\EnvData;
use Manager\Erp\Model\Export\DataSaver;
use Psr\Log\LoggerInterface;

abstract class GridTo extends Action
{
    private EnvData $config;
    private DataSaver $saver;
    private Filter $filter;
    private LoggerInterface $logger;
    private array $collections = [];
    private array $collectionFactories = [];

    /**
     * @param Context $context
     * @param DataSaver $saver
     * @param EnvData $config
     * @param ?Filter $filter
     * @param ?LoggerInterface $logger
     */
    public function __construct(
        Context          $context,
        DataSaver        $saver,
        EnvData          $config,
        ?Filter          $filter = null,
        ?LoggerInterface $logger = null
    )
    {
        parent::__construct($context);
        $this->config = $config;
        $this->saver = $saver;
        $this->filter = $filter ?? $this->_objectManager->get(Filter::class);
        $this->logger = $logger ?? $this->_objectManager->get(LoggerInterface::class);
        $this->collectionFactories = [
        "order" => $this->_objectManager->get(OrderCollectionFactory::class),
        "invoice" => $this->_objectManager->get(InvoiceCollectionFactory::class)
        ];
    }


    /**
     * Create a ResourceModel Collection
     * @return AbstractCollection
     */
    private function getCollection(): AbstractCollection
    {
        if (!isset($this->collections[$this->getFilePrefix()])) {
            $this->collections[$this->getFilePrefix()] = $this->collectionFactories[$this->getFilePrefix()]->create()
                ->addFieldToSelect($this->getColumnNames());
        }
        /*echo "<pre>";
        var_dump(
            $this->collections[$this->getFilePrefix()]->getIdFieldName()
        );
        echo "</pre>";
        die();*/
        return $this->collections[$this->getFilePrefix()];
    }

    /**
     * Get if exported file contiens headers
     * @return bool
     */
    private function exportWithHeader(): bool
    {
        return match ($this->getFilePrefix()) {
            "order" => $this->config->exportOrderWithHeader(),
            "invoice" => $this->config->exportInvoiceWithHeader()
        };
    }

    /**
     * Get Headers from configuration if existe
     * @return string[]|null
     */
    private function getConfigHeaders(): array|null
    {
        if ($this->exportWithHeader()) {
            return match ($this->getFilePrefix()) {
                "order" => $this->config->getOrderHeaders(),
                "invoice" => $this->config->getInvoiceHeaders()
            };
        }
        return null;
    }

    /**
     * get file's headers if existe
     * @return ?string[]
     */
    private function getHeaders(): ?array
    {
        return $this->getConfigHeaders() ? array_values($this->getConfigHeaders()) : null;
    }

    /**
     * get file name prefix (order|invoice), null if not exsite
     * @return ?string
     */
    private function getFilePrefix(): ?string
    {
        $filePrefix = explode("_", $this->_request->getParam("namespace"))[0];
        return !empty($filePrefix) ? $filePrefix : null;
    }

    /**
     * Get columns name of Table
     * @return string[]
     */
    private function getColumnNames(): array
    {
        return match ($this->getFilePrefix()) {
            "order" => $this->config->getOrderExportColumn(),
            "invoice" => $this->config->getInvoiceExportColumn()
        };
    }

    /**
     * Get Collection by ids
     * @param array $ids table row's id
     * @return AbstractCollection
     */
    public function filterCollectionByIds(array $ids):AbstractCollection
    {
        return $this->getCollection()->addFieldToFilter(
            $this->getCollection()->getIdFieldName(),
            $ids
        )->addAttributeToSort($this->getCollection()->getIdFieldName(), 'asc');
    }

    /**
     * @return ResponseInterface
     */
    public function execute(): ResponseInterface
    {
        if (!$this->getFilePrefix() || !in_array($this->getFilePrefix(), ['order', 'invoice'])) {
            $this->messageManager->addErrorMessage(__("bad request"));
            return $this->_redirect($this->_redirect->getRefererUrl());
        }

        if ($this->config->isEnabled()) {
            try {
                $component = $this->filter->getComponent();
                $this->filter->prepareComponent($component);
                $this->filter->applySelectionOnTargetProvider();

                $ids = array_map(
                    fn($item) => $item->getId(),
                    $component->getContext()->getDataProvider()->getSearchResult()->getItems()
                );
                $collection = $this->filterCollectionByIds($ids);

                $data['header'] = $this->getHeaders();

                foreach ($collection as $item ){
                    $data['content'][] =  array_map(
                        fn($columnName)=> $item->getData($columnName),
                        $this->getColumnNames()
                    );
                }

                $folderName = $this->config->getFolderName();
                $fileName = $this->getFilePrefix() . "_" . uniqid();
                if ($this->saver->save(["fileName" => $fileName, "directory" => $folderName], $data))
                {
                    $this->messageManager->addSuccessMessage(
                        $fileName . __(' exported to: ') . $folderName
                    );
                } else {
                    $this->_response->setStatusCode(500);
                    $this->messageManager->addErrorMessage(
                        __('access denied to: ') . $folderName
                    );
                }
            }
            catch (LocalizedException $e){
                $this->logger->critical($e->getMessage());
                $this->messageManager->addErrorMessage( __("Component not Found"));

            }
        } else {
            $this->messageManager->addErrorMessage('Manager_Erp ' . __("not enabled"));
        }
        return $this->_redirect($this->_redirect->getRefererUrl());
    }

    /**
     * Checking if the user has access to requested component.
     *
     * @inheritDoc
     */
    protected function _isAllowed(): bool
    {
        if ($this->_request->getParam('namespace')) {
            try {
                $component = $this->filter->getComponent();
                $dataProviderConfig = $component->getContext()
                    ->getDataProvider()
                    ->getConfigData();
                if (isset($dataProviderConfig['aclResource'])) {
                    return $this->_authorization->isAllowed(
                        $dataProviderConfig['aclResource']
                    );
                }
            } catch (Exception $exception) {
                $this->logger->critical($exception);

                return false;
            }
        }

        return true;
    }
}
