<?php

namespace Manager\Erp\Controller\Adminhtml\Log;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Manager\Erp\Model\Log as LogModel;
use Manager\Erp\Model\LogFactory as LogModelFactory;
use Manager\Erp\Model\ResourceModel\Log as LogResourceModel;
use Manager\Erp\Model\ResourceModel\LogFactory as LogResourceModelFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\Escaper;
class Delete extends Action implements HttpGetActionInterface
{
    private LogModel $logModel;
    private LogResourceModel $logResourceModel;
    private Redirect $redirect;
    private Filesystem $filesystem;
    private LoggerInterface $logger;
    private Escaper $escaper;
    public function __construct(Context $context, LoggerInterface $logger, LogModelFactory $logFactory, LogResourceModelFactory $logResourceModelFactory, Filesystem $filesystem)
    {
        parent::__construct($context);
        $this->logModel = $logFactory->create();
        $this->logResourceModel = $logResourceModelFactory->create();
        $this->redirect = $this->resultRedirectFactory->create();
        $this->filesystem = $filesystem;
        $this->logger = $logger;
        $this->escaper = $this->_objectManager->get(Escaper::class);
    }

    /**
     * @inheritDoc
     */
    public function execute(): Redirect
    {
        $id = $this->getRequest()->getParam('id');
        if(isset($id)){
            try {
                $directory = filesystem->getDirectoryWrite(DirectoryList::ROOT);
                $this->logResourceModel->load($this->logModel, $id);
                $fileName = $this->logModel->getData("file_name");
                $filePath = $this->logModel->getData("file_path");
                $path = $filePath . $fileName;
                if ($directory->isFile($path)) {
                    $directory->delete($path);
                }
                $this->logResourceModel->delete($this->logModel);
                $this->messageManager->addSuccessMessage($fileName . __(' was deleted'));
            } catch (Exception $e) {
                $this->logger->critical($e->getMessage());
                $this->messageManager->addErrorMessage(__("Error: Can't delete file with id:" . $this-$this->escaper->escapeHtml($id)));
            }
        }
        else{
            $this->messageManager->addErrorMessage(__("Error: id param not Found"));
        }
        return $this->redirect->setPath('*/*/');
    }

    /**
     * Checking if the user has access to requested component.
     *
     * @inheritDoc
     */
    protected function _isAllowed(): bool
    {
        return $this->_authorization->isAllowed('Manager_Erp::Log');
    }
}
