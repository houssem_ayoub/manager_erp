<?php

namespace Manager\Erp\Controller\Adminhtml\Export;

use Exception;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Manager\Erp\Controller\GridTo;
use Manager\Erp\Helper\EnvData;
use Manager\Erp\Model\Export\CSVSaver;
use Psr\Log\LoggerInterface;

class GridToCsv extends GridTo
{
    /**
     * @param Context $context
     * @param CSVSaver $saver
     * @param EnvData $config
     * @param ?Filter $filter
     * @param ?LoggerInterface $logger
     * @throws Exception
     */
    public function __construct(
        Context         $context,
        CSVSaver        $saver,
        EnvData    $config,
        ?Filter          $filter = null,
        ?LoggerInterface $logger = null
    ) {
        parent::__construct($context, $saver, $config, $filter, $logger);
    }
}
