<?php

namespace Manager\Erp\Controller\Adminhtml\Export;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Manager\Erp\Controller\GridTo;
use Manager\Erp\Helper\EnvData;
use Manager\Erp\Model\Export\XMLSaver;
use Psr\Log\LoggerInterface;

class GridToXml extends GridTo
{
    /**
     * @param Context $context
     * @param XMLSaver $saver
     * @param EnvData $config
     * @param ?Filter $filter
     * @param ?LoggerInterface $logger
     */
    public function __construct(
        Context         $context,
        XMLSaver        $saver,
        EnvData    $config,
        ?Filter          $filter = null,
        ?LoggerInterface $logger = null
    ) {
        parent::__construct($context, $saver, $config, $filter, $logger);
    }

}
