<?php

namespace Manager\Erp\Controller\Adminhtml\Export;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Manager\Erp\Helper\EnvData;

class Test extends Action
{


    private EnvData $envData;

    public function __construct(Context $context, EnvData $envData)
    {
        parent::__construct($context);
        $this->envData = $envData;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        echo "<pre>";
        var_dump($this->envData->getOrderHeaders());
        //print_r(->getAllItems());
        echo "</pre>";
        exit(0);
    }
}
