<?php

namespace Manager\Erp\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Manager\Erp\Model\ResourceModel\Log as ResourceModel;

class Log extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'manager_erp_log';

    protected $_cacheTag = 'manager_erp_log';
    protected $_eventPrefix = 'manager_erp_log';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(ResourceModel::class);
    }

    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
