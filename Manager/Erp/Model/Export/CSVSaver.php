<?php

namespace Manager\Erp\Model\Export;

class CSVSaver extends ADataSaver
{
    protected function saveData($stream, $data = []): void
    {
        if (is_array($data['header'])) {
            $stream->writeCsv($data['header']);
        }
        if (is_array($data['content'])) {
            foreach ($data['content'] as $row) {
                $stream->writeCsv($row);
            }
        }
    }

    protected function getExtension(): string
    {
        return ".csv";
    }
}
