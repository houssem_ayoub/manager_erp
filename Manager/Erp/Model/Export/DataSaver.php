<?php

namespace Manager\Erp\Model\Export;

interface DataSaver
{
    public function save(
        array $file = [
            "fileName" => "file",
            "directory" => ""
        ],
        array $data = []
    ):bool;
}
