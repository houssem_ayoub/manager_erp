<?php

namespace Manager\Erp\Model\Export;

use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\UrlInterface;
use Manager\Erp\Model\LogFactory as LogModelFactory;
use Manager\Erp\Model\ResourceModel\LogFactory as LogResourceFactory;
use Psr\Log\LoggerInterface;

abstract class ADataSaver implements DataSaver
{
    private LogModelFactory $logModelFactory;
    private LogResourceFactory $logResourceFactory;
    private UrlInterface $url;
    private Filesystem $filesystem;
    private LoggerInterface $logger;

    public function __construct(
        Filesystem $filesystem,
        UrlInterface $url,
        LoggerInterface $logger,
        LogModelFactory $logModelFactory,
        LogResourceFactory $logResourceFactory
    ) {
        $this->filesystem = $filesystem;
        $this->logger = $logger;
        $this->url = $url;
        $this->logModelFactory = $logModelFactory;
        $this->logResourceFactory = $logResourceFactory;
    }
    abstract protected function saveData($stream, $data = []): void;

    /**
     * Get File extension
     * @return string
     */
    abstract protected function getExtension():string;

    /**
     * Save data on File
     * @param array $file [fileName:sting,directory:string]
     * @param array $data [header: null | string[], content : (number|string)[][]]
     * @return bool true if operation succeed, false else
     */
    public function save(array $file = ['directory'=>""], array $data = []): bool
    {
        try {
            $directory = $this->filesystem->getDirectoryWrite(DirectoryList::PUB);
            $fileName = ($file["fileName"] ?? "file") . $this->getExtension();
            $directoryNane = !empty($file["directory"]) ? $file["directory"] . DIRECTORY_SEPARATOR : "";
            $path = $directoryNane . $fileName;
            $stream = $directory->openFile($path, 'w+');
            $stream->lock();
            $this->saveData($stream, $data);
            $stream->unlock();
            $stream->close();
            $logModel = $this->logModelFactory->create();
            $logResource = $this->logResourceFactory->create();
            $logResource->load($logModel, $fileName, "file_name");
            $logModel->setFileName($fileName);
            $logModel->setFilePath(DirectoryList::PUB . DIRECTORY_SEPARATOR . $directoryNane);
            $logModel->setLink($this->url->getDirectUrl($path));
            $logResource->save($logModel);
            return true;
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
            return false;
        }
    }
}
