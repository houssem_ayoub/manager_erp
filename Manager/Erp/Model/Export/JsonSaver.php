<?php

namespace Manager\Erp\Model\Export;

use Magento\Framework\Filesystem;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\UrlInterface;
use Manager\Erp\Model\LogFactory as LogModelFactory;
use Manager\Erp\Model\ResourceModel\LogFactory as LogResourceFactory;
use Psr\Log\LoggerInterface;

class JsonSaver extends ADataSaver
{
    private SerializerInterface $serializer;


    public function __construct(
        Filesystem          $filesystem,
        SerializerInterface $serializer,
        LoggerInterface $logger,
        LogModelFactory     $logModelFactory,
        LogResourceFactory  $logResourceFactory,
        UrlInterface        $url
    ) {
        parent::__construct($filesystem, $url, $logger, $logModelFactory, $logResourceFactory);
        $this->serializer = $serializer;
    }

    protected function saveData($stream, $data = []): void
    {
        $content = [];
        if (is_array($data['header'])) {
            $content[] = $data['header'];
        }
        if (is_array($data['content'])) {
            $content = array_merge($content, $data['content']);
        }
        $stream->write($this->serializer->serialize($content));
    }

    /**
     * @inheirtDoc
     */
    protected function getExtension(): string
    {
        return ".json";
    }
}
