<?php

namespace Manager\Erp\Model\Export;

use ArrayObject;
use Magento\Framework\Convert\ExcelFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\UrlInterface;
use Manager\Erp\Model\LogFactory as LogModelFactory;
use Manager\Erp\Model\ResourceModel\LogFactory as LogResourceFactory;
use Psr\Log\LoggerInterface;

class XMLSaver extends ADataSaver
{
    private ExcelFactory $excelFactory;

    public function __construct(
        LogModelFactory     $logModelFactory,
        LogResourceFactory  $logResourceFactory,
        UrlInterface        $url,
        LoggerInterface $logger,
        Filesystem $filesystem,
        ExcelFactory $excelFactory
    ) {
        parent::__construct($filesystem, $url, $logger, $logModelFactory, $logResourceFactory);
        $this->excelFactory = $excelFactory;
    }

    protected function saveData($stream, $data = []): void
    {
        $arrayObject = new ArrayObject($data['content']);
        $excel = $this->excelFactory->create([
            'iterator' =>  $arrayObject->getIterator(),
        ]);
        if (isset($data['header'])) {
            $excel->setDataHeader($data['header']);
        }
        $excel->write($stream, "sheet1");
    }

    protected function getExtension(): string
    {
        return ".xml";
    }
}
