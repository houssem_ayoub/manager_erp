<?php

namespace Manager\Erp\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class FileTypeOption implements OptionSourceInterface
{

    /**
     * @inheritDoc
     */
    public function toOptionArray():array
    {
        return [
            [
                'value'=>0,
                'label'=>__("CSV")
            ], [
                'value'=>1,
                'label'=>__("XML")
            ], [
                'value'=>2,
                'label'=>__("JSON")
            ],
        ];
    }

    public function toArray():array
    {
        return [0 => __("CSV"), 1 => __("XML"), 2 => __("JSON")];
    }
}
