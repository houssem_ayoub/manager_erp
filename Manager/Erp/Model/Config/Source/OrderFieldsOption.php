<?php

namespace Manager\Erp\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\ResourceModel\Order;
use Magento\Sales\Model\ResourceModel\OrderFactory;
use Psr\Log\LoggerInterface;

class OrderFieldsOption implements OptionSourceInterface
{
    private Order $order;
    private LoggerInterface $logger;

    public function __construct(OrderFactory $factory, LoggerInterface $logger)
    {
        $this->order = $factory->create();
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray(): array
    {
        $items = $this->toArray();
        return array_map(fn ($v) => ['value' => $v, 'label' => $v], $items);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        try {
            return array_keys($this->order->getConnection()->describeTable($this->order->getMainTable()));
        } catch (LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            return [];
        }
    }
}
