<?php

namespace Manager\Erp\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\ResourceModel\Order\Invoice;
use Magento\Sales\Model\ResourceModel\Order\InvoiceFactory;
use Psr\Log\LoggerInterface;
class InvoiceFieldsOption implements OptionSourceInterface
{
    private Invoice $invoice;
    private LoggerInterface $logger;

    public function __construct(InvoiceFactory $factory, LoggerInterface $logger)
    {
        $this->invoice = $factory->create();
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray(): array
    {
        $items = $this->toArray();
        return array_map(fn ($v) =>['value'=>$v,'label'=>$v], $items);
    }


    /**
     * @return array
     */
    public function toArray():array
    {
        try {
            return array_keys($this->invoice->getConnection()->describeTable($this->invoice->getMainTable()));
        } catch (LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            return [];
        }
    }
}
