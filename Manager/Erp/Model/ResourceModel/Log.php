<?php

namespace Manager\Erp\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Log extends AbstractDb
{
    /**
     * @var string
     */
    protected string $_eventPrefix = 'manager_erp_log_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('erp_log', 'id');
        $this->_useIsObjectNew = true;
    }
}
