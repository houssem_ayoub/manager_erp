<?php

namespace Manager\Erp\Model\ResourceModel\Log;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Manager\Erp\Model\Log as Model;
use Manager\Erp\Model\ResourceModel\Log as ResourceModel;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'manager_erp_log_collection';
    protected $_eventObject = 'log_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
