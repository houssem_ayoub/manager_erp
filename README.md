# Manager Erp Setup
## Project Require
* PHP >= 8.1
* Magento = 2.5.1

## Add module to your project
```bash
cp Manager {project Name}/app/code
```
## Install module
```php
php bin/magento setup:upgrade
php bin/magento module:enable Manager_Erp
php bin/magento setup:di:compile
```
## Enable module
https://{domain}/{admin}/admin/system_config/
* ERP Manager Configuration
  * Erp Configuration : Set Enabled to true

![](https://gitlab.com/houssem_ayoub/manager_erp/-/raw/main/screenshots/0.png)
![](https://gitlab.com/houssem_ayoub/manager_erp/-/raw/main/screenshots/1.png)

_By default, the export folder is 'erp'_
## Export Orders from menu
* Go to **ERP EXPORT** menu item
* Press **Export Orders**

![](https://gitlab.com/houssem_ayoub/manager_erp/-/raw/main/screenshots/3.png)
* Select items to export if you will not export it all
* Choice export type **[CSV, XML, JSON]** and press **Export** button

![](https://gitlab.com/houssem_ayoub/manager_erp/-/raw/main/screenshots/4.png)
* A flash message notifies you that export was succeed with the **file name**

![](https://gitlab.com/houssem_ayoub/manager_erp/-/raw/main/screenshots/5.png)
* the new file will be added to the log table **erp_log**

![](https://gitlab.com/houssem_ayoub/manager_erp/-/raw/main/screenshots/6.png)
  * You can access to file with:

`https://{domain}/{export folder name}/{filename}.{extension}`

  ![](https://gitlab.com/houssem_ayoub/manager_erp/-/raw/main/screenshots/7.png)
